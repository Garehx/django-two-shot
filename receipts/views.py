from django.shortcuts import redirect, render
from receipts.forms import AccountForm, ExpenseForm, ReceiptForm
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def all_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "all_receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.purchaser = request.user
            recipe.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": expense,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            yeehaw = form.save(False)
            yeehaw.owner = request.user
            yeehaw.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_account.html", context)
